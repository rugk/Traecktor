import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import text_de from "./de/text.json"
import commmon_de from "./de/common.json"
import text_en from "./en/text.json"

i18n

    .use(initReactI18next)
    .init({
        lng: 'de',
        fallbackLng: 'de',
        debug: true,
        interpolation: {
            escapeValue: false,
        },
        ns: ['common', 'text'],
        defaultNS: 'text',
        resources: {
            de: {
                text: text_de,
                common: commmon_de
            },
            en: {
                text: text_en
            }
        }
    });


export default i18n;