import { createPortal } from "react-dom";

export function Portal(props: { children: any }) {

    const rootElement = document.querySelector(`#portal`) || document.createElement("div");
    
    return createPortal(props.children, rootElement);

}