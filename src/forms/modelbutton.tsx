import React, { useState } from 'react';
import { Portal } from './portal';


export default function (props: {
	buttonLabel: string,
	modalTitle: string,
	className: string,
	children?: JSX.Element[]
}) {

	const [enabled, setEnabled] = useState(false);
	const [show, setShow] = useState(false);

	function showModal (){
		setEnabled(true);
		setTimeout(() => {
			setShow(true);
		}, 100);
	}

	function hideModal ()  {
		setShow(false);
		setTimeout(() => {
			setEnabled(false);
		}, 100);
	}

	return (

		<>

			<button type="button" className={props.className} onClick={showModal}>
				{props.buttonLabel}						
			</button>

			<Portal>
				<div className={"modal fade " + ((show) ? "show" : "")} tabIndex={-1} role="dialog" style={(enabled) ? {
					paddingRight: "17px",
					display: "block"
				} : {}}>
					<div className="modal-dialog modal-xl">
						<div className="modal-content">
						<div className="modal-header">
							<h5 className="modal-title">{props.modalTitle}</h5>
							<button type="button" className="close" onClick={hideModal}>
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div className="modal-body">
							{(props.children) ? props.children : null}
						</div>
						</div>
					</div>
				</div>

				{(show) ? (<div className="modal-backdrop fade show" onClick={hideModal}></div>) : null}
			</Portal>

		</>
	)

}