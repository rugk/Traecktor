import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

export default function (props: {
    options: {
		value: string,
		body?: string | any,
		label: string | any,
		filterText?: string,
		after?: any
	}[],
	checked: string[],
	onChange: {(checked: string[]): void}
}) {

	const [t] = useTranslation();

	const [filterBy, setFilterBy] = useState("");

	function onChange (changeEvent: any) {

		let checked = props.checked;
		const value: string = changeEvent.target.value;
		const indexOf = checked.indexOf(value);

		if (indexOf > -1) checked.splice(indexOf, 1)
		else checked.push(value);

		props.onChange(checked);

	}

	const id = "checkbox_" + Math.random();

	const options = props.options.filter(e => (!e.filterText || e.filterText.toLowerCase().indexOf(filterBy.toLowerCase()) > -1));

	return (
		<div className="checkboxes">
			{(props.options.length > 5) ? (
				<>
					<input className="form-control" placeholder={t("tracker-gen.input.filter-placeholder")} value={filterBy} onChange={event => setFilterBy(event.target.value)}/>
					<br />
				</>
			) : null}
			<div className="options">
				{options.map((e: any, index: number) => (
					<div key={Math.random()} className="form-check">
						<input checked={(props.checked.indexOf(e.value) > -1)} value={e.value} onChange={onChange} className="form-check-input" type="checkbox" id={id + index} />
						<label className="form-check-label title" htmlFor={id + index}>
							{e.label}
						</label>
						{e.body}
						{(e.after) ? e.after : null}
					</div>
				))}
			</div>
		</div>
	)


}
