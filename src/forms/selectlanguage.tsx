import React from 'react';
// import { useTranslation } from "react-i18next"

import "../style/navigation.scss"

export default function (props: { 
    locale: string,
    simple?: boolean
    changeLanguage: {(event: React.ChangeEvent<HTMLSelectElement>): void}
} ) {

    // const [t] = useTranslation();

    const translations = [
        { id: "de", title: "Deutsch" },
        { id: "en", title: "English" },
    ]

    return (
        <div className="locale-switcher">
            <select value={props.locale} onChange={props.changeLanguage}>
                {translations.map((trans, key: number) => (
                    <option key={key} value={trans.id}>{trans.title}</option>
                ))}
            </select>
        </div>
    )
}