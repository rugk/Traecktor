import React from 'react';

export default (props: {
	onChange: {(value: string): void},
	label?: string,
	children?: any
	value: string,
	checkedValue: string,
	checkedLabel?: string | null
}) => {

	let checked = false;
	if (props.checkedValue === props.value) checked = true;

	const randId: string = String(Math.random());

	return (
		<div className="form-check">
			<input className="form-check-input" type="radio" value={props.value} checked={checked} id={randId} onChange={(event: any)=>{props.onChange(event.target.value)}} />
			<label className="form-check-label" htmlFor={randId}>
				{(props.label) ? props.label : null}
				{(props.children) ? props.children : null}
				{(checked && props.checkedLabel) ? (
					<div style={{ color: "red" }}>{props.checkedLabel}</div>
				) : null}
			</label>
		</div>
	)

}