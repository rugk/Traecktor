
import dsb from "./data/dsb_de.json"
import plzToDsb from "./data/plz_to_dsb_de.json"

import * as ics from 'ics'
import moment from 'moment'

export const getDSBByPLZ = (plz: number) => {

    let foundDSB = null;

    plzToDsb.forEach((element: { from: number, to: number, dsbid: number}) => {
        if (element.from <= plz && element.to > plz) {
            console.log("Found", element);
            foundDSB = dsb.find((e: { id: number }) => e.id === element.dsbid);
        }
    });

    return (foundDSB) ? foundDSB : null;
        
}

export const getHostname = (url: string) => {
	const urlParts = url.replace('http://','').replace('https://','').split(/[/?#]/);
	return urlParts[0]
}

export const downloadFile = (name: string, body: string, content: string = "text/calendar") => {

    var element = document.createElement('a');
    element.setAttribute('href', `data:${content};charset=utf-8,` + encodeURIComponent(body));
    element.setAttribute('download', name);
  
    element.style.display = 'none';
    document.body.appendChild(element);
  
    element.click();
  
    document.body.removeChild(element);

}

export const copy = (str: string) => {
    if (navigator.clipboard) navigator.clipboard.writeText(str);
}

function replaceLast (str: string, find: string, replace: string) {
    var index = str.lastIndexOf(find);

    if (index >= 0) {
        return str.substring(0, index) + replace + str.substring(index + find.length);
    }

    return str.toString();
}

export const convertArrayToOrList = (list: string[]) => {

    const listStr = list.join(", ");

    return replaceLast(listStr, ", ", ' oder ');

}

export function generateICSFile (t: any, trackerUrl: string): void {

    const domain = getHostname(trackerUrl);

    const event: any = {
        start: moment().add({ 'weeks': 4 }).format("YYYY-M-D-H-m").split("-"),
        duration: { minutes: 30 },
        title: t("tracker-gen.reminder.title", { domain }),
        description: t("tracker-gen.reminder.description", { domain }),
        location: 'https://träcktor.de/'
    }
       
    ics.createEvent(event, (error, value) => {
        if (!error) downloadFile(t("tracker-gen.reminder.filename", { domain }) + ".ics", value);
    })

}