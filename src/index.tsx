import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import {
    HashRouter as Router,
    Switch,
    Route
} from "react-router-dom";

import './locale/i18n';

import FAQ from "./components/faq"
import Navigation from "./components/navigation"
import Footer from "./components/footer"
import ComplaintGenerator from "./components/complaintgenerator"

import "./style/bootstrap.min.css"
import "./style/main.scss"
import { useTranslation } from 'react-i18next';
import Tracker from './components/tracker';


function App () {

    const [locale, setLocale] = useState("de");
    const [t, i18n] = useTranslation();

    const changeLanguage = (event: React.ChangeEvent<HTMLSelectElement>) => {

        const newLocale = event.target.value;

        i18n.changeLanguage(newLocale);

        setLocale(newLocale);
        
    }

    return (

        <Router>

            <Navigation locale={locale} changeLanguage={changeLanguage}/>

            <div style={{minHeight: "calc(100vh - 350px)"}}>

            <Switch>
                <Route path="/faq">
                    <FAQ />
                </Route>
                <Route path="/tracker">
                    <Tracker />
                </Route>
                <Route path="/">
                    
                    <div className="content">
                        <h3 className="slogan linebreaks">{t("slogan")}</h3>

                        <ComplaintGenerator locale={locale} />

                    </div>

                </Route>
            </Switch>
            </div>

            <Footer locale={locale} changeLanguage={changeLanguage}/>
            
        </Router>

    )

}

ReactDOM.render(<App />, document.getElementById('root'));