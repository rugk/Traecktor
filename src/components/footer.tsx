import React from 'react';
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next"

import "../style/footer.scss"

import SelectLanguage from "../forms/selectlanguage"


export default function (props: { 
    locale: string,
    changeLanguage: {(event: React.ChangeEvent<HTMLSelectElement>): void}
}) {

    const [t] = useTranslation();

    return (
        <div className="footer">

            <div className="container">
                <div className="infobox big">

                    <p className="linebreaks">
                        {t("footer-slogan")}
                    </p>

                    <SelectLanguage locale={props.locale} changeLanguage={props.changeLanguage} />
                    <ul className="line">
                        <li><Link to="/faq">{t("navigation.tutorial-faq")}</Link></li> 
                        <li><Link to="/tracker">{t("navigation.tracker-database")}</Link></li> 
                        <li><a href="https://rufposten.de/blog/kontakt/">{t("navigation.imprint")}</a></li>
                    </ul>
                    
                </div>
                <div className="infobox">
                    <span className="title">{t("navigation.community")}</span>
                    <ul>
                        <li>
                            <a href="https://codeberg.org/rufposten/Traecktor">{t("navigation.sourcecode")}</a>
                        </li>
                        <li>
                            <a href="https://codeberg.org/rufposten/Traecktor/issues">{t("navigation.add-tracker")}</a>
                        </li>
                        <li>
                            <a href="https://codeberg.org/rufposten/Traecktor/src/branch/dev/src/locale">{t("navigation.help-with-translation")}</a>
                        </li>    
                        <li>
                            <a href="https://rufposten.de/blog/spenden/">{t("navigation.donate")} <span role="img" aria-label="Herz">❤️</span></a>
                        </li>
                    </ul>
                </div>
            </div>

            


        </div>
    )
}