import React from 'react';

import { useTranslation } from 'react-i18next';

import "../style/faq.scss"

export default function () {

    const [t] = useTranslation();

    const faq: [] = t('faq.questions', { returnObjects: true });

    return (

        <div className="faq">

            <h2>{t('faq.tutorial-title')}</h2>
            <figure className="tutorialvideo">
                <video controls poster="https://rufposten.de/blog/wp-content/uploads/2020/06/traecktor_videoposter_moz.png" src="https://rufposten.de/blog/wp-content/uploads/2020/06/traecktor_tutorial_720.mp4"></video>
                <figcaption style={{textAlign: "center"}}>{t('faq.tutorial-desc')}</figcaption>
            </figure>

            <h2>{t('faq.title')}</h2>

            {(faq.map((e: { question: string, answer: string }) => (
                <div key={Math.random()}>
                    <h3>{e.question}</h3>
                    <p dangerouslySetInnerHTML={{__html: e.answer}}></p>
                </div>
            )))}

        </div>

    )

}