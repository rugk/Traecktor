import React, { useState } from 'react'

import moment from 'moment'

import "../style/generator.scss"

import RadioButton from "../forms/radiobutton"
import CheckBoxes from "../forms/checkboxes"
import ModalButton from "../forms/modelbutton"

import { getDSBByPLZ, getHostname, generateICSFile } from "../utils"
import { useTranslation } from 'react-i18next'

import dsb_eu from "../data/dsb_eu.json"

import {ITrackers, ITracker} from "./tracker"
import { getTrackerItems } from './complaintgenerator/trackeritem'

interface DSB {
	internet: string,
	email: string,
	title: string,
	id?: number
}


export default function (props: {locale: string}) {

	const [t] = useTranslation();

	const noDSBfound: DSB = {
		id: -1,
		title: "?",
		internet: "",
		email: t("tracker-gen.dsb-email-placeholder")
	}
	
	const Trackers: ITrackers = t('tracker', { returnObjects: true });

	const [cnameCloakingDomains, setCnameCloakingDomains] = useState(([ ] as {trackerTitel: string, domain: string, lastChanged: boolean}[]));

	const [clickedAnAction, setClickedAnAction] = useState(false);
	const [beschwerdeLand] = useState("Deutschland");
	const [generatorTyp, setGeneratorTyp] = useState("request"); // request, complaint
	const [zustimmungsTyp, setZustimmungsTyp] = useState("ja"); // ja, nein, minimal
	const [trackerUrl, setTrackerUrl] = useState("http://");
	const [adresseBetroffener, setAdresseBetroffener] = useState("");
	const [adresseSeitenbetreiber, setAdresseSeitenbetreiber] = useState("");
	const [emailDatenschutzbeauftragter, setEmailDatenschutzbeauftragter] = useState("");
	const [lastCheckDate, setLastCheckDate] = useState(moment().format('DD.MM.YYYY') + " um ca. " + moment().format('HH') + ":00 Uhr");
	const [checkedTracker, setCheckedTracker] = useState(([] as number[]));
	const [checkedBelege, setCheckedBelege] = useState(([] as string[]));
	const [selectedDSB, setSelectedDSB] = useState(noDSBfound);

	let generatedText: string = "";

	const isBeschwerde = (): boolean => generatorTyp === "complaint";
	const getActiveToEmail = (): string => (isBeschwerde()) ? selectedDSB.email : emailDatenschutzbeauftragter;

	function getSubject (): string  {
		return t(`tracker-gen.${((isBeschwerde()) ? "complaint" : "request") }.subject`, { 
			domain: getHostname(trackerUrl)
		})
	}

	function updateDSB (land: string, adresse: string = adresseSeitenbetreiber) {

		let found: DSB | null | undefined = noDSBfound;

		if (land === "Deutschland") {

			const foundPLZ = adresse.match(/[0-9]{5}/);
			const plz: number = parseInt((foundPLZ) ? foundPLZ[0] : "0");

			found = getDSBByPLZ(plz);

		} else {
			found = dsb_eu.find(e => e.title === land);
		}

		if (!found) setSelectedDSB(noDSBfound)
		else {
			setSelectedDSB(found);
		}

	}

	function changeBetreiberAdresse (event: React.ChangeEvent<HTMLTextAreaElement>): void {

		setAdresseSeitenbetreiber(event.target.value);
		updateDSB(beschwerdeLand, event.target.value);

	}

	// function changeBeschwerdeLand (event: React.ChangeEvent<HTMLSelectElement>): void {

	// 	setBeschwerdeLand(event.target.value);
	// 	updateDSB(event.target.value);	

	// }



	// -----------------------------------
	// Aktionen

	function openEmailProgram ():void {
        (window as any).location.href = `mailto:${getActiveToEmail()}?subject=${encodeURIComponent(getSubject())}&body=${encodeURIComponent(generatedText)}`;
		setClickedAnAction(true);
	}

	function copyGenTextToClipboard (): void {
		
		if (navigator.clipboard)
			navigator.clipboard.writeText(generatedText);

		setClickedAnAction(true);
			
	}

	// ---------------------------------------
	// Aufforderungstext + Beschwerdetext

	function getTrackerText (): string {

		let trackers = Trackers.filter((tracker: ITracker, index: number) => checkedTracker.indexOf(index) > -1);

		return trackers.map((tracker: ITracker, index: number) => {
			let text = (isBeschwerde()) ? tracker.complaint : tracker.request;

			if (tracker.cnameCloaking) {
				text = text.replace("{cnameCloakingDomain}", cnameCloakingDomains.find(e => e.trackerTitel === tracker.title)?.domain || "");
			}

			return text;
			
		}).join("\n\n");

	}

	function getRequestText (): JSX.Element {

		const newGeneratedText = t("tracker-gen.request.text", {
			trackerUrl,
			trackerText: getTrackerText(),
			checkedTracker: checkedTracker.length,
			consentText: t(`tracker-gen.request.consent-text-${zustimmungsTyp}`),
			addressOfThePersonConcerned: adresseBetroffener
		})

		generatedText = newGeneratedText
			.replace(/<\/div>/g, "")
			.replace(/<div(.*?)>/g, "");

		return (
			<div dangerouslySetInnerHTML={{__html: newGeneratedText.split("\n").join("<br />")}}></div>
		)
		
	}

	function getComplaintText (): JSX.Element {

		let documentText = "";
		const gesamt = checkedBelege.length;

		for (let n = 0; n < checkedBelege.length; n++) {
			if (n > 0 && gesamt - n > 1 && gesamt > 1) documentText += ", ";
			else if (gesamt-n === 1 && gesamt>1) documentText += " " + t("common:and") + " ";
			documentText += checkedBelege[n];
		}

		const newGeneratedText = t("tracker-gen.complaint.text", {
			trackerUrl,
			lastCheckDate,
			documentText,
			addressSiteOperator: adresseSeitenbetreiber,
			trackerText: getTrackerText(),
			checkedTracker: checkedTracker.length,
			consentText: t(`tracker-gen.complaint.consent-text-${zustimmungsTyp}`),
			addressOfThePersonConcerned: adresseBetroffener
		})
	
		generatedText = newGeneratedText
			.replace(/<\/div>/g, "")
			.replace(/<div(.*?)>/g, "");

		return (
			<div dangerouslySetInnerHTML={{__html: newGeneratedText.split("\n").join("<br />")}}></div>
		)
		
	}

	function updateCnameCloakingDomains (event: React.ChangeEvent<HTMLInputElement>) {

		const domainId = parseInt(event.target.dataset["domainid"] || "0");
		const tracker = Trackers[domainId];
		const cnameDomain = event.target.value;

		const updateDomain = cnameCloakingDomains.find(e => e.trackerTitel === tracker.title);
		if (!updateDomain) cnameCloakingDomains.push({trackerTitel: tracker.title, domain: cnameDomain, lastChanged: true});

		setCnameCloakingDomains(cnameCloakingDomains.map((cname) => {
			cname.lastChanged = false;
			if (cname.trackerTitel === tracker.title) {
				cname.domain = cnameDomain;
				cname.lastChanged = true;
			}
			return cname;
		}));

	}

	return (

		<div className="generator">

			<div className="gen-input">

				<h3>{t("tracker-gen.input.title")}</h3>
				<p>{t("tracker-gen.input.desc")}</p>

				<h5>{t("tracker-gen.input.section-action.title")}</h5>
				<RadioButton onChange={setGeneratorTyp} value="request" label={t("tracker-gen.input.section-action.radio-request")} checkedValue={generatorTyp} />
				<RadioButton onChange={setGeneratorTyp} value="complaint" label={t("tracker-gen.input.section-action.radio-complaint")} checkedValue={generatorTyp} />

				<br />

				<h5>{t("tracker-gen.input.section-trackerurl.title")}</h5>
				<input type="text" className="form-control" id="url" value={trackerUrl} onChange={(event: any) => { setTrackerUrl(event.target.value) }} />
				<br />


				{(isBeschwerde()) ? (

					<div className="textfrage">

						<h5>{t("tracker-gen.input.section-address-site-operator.title")}</h5>
						<p>{t("tracker-gen.input.section-address-site-operator.desc")}</p>
						{/* <p>
							<select className="form-control" value={beschwerdeLand} onChange={changeBeschwerdeLand}>
								{dsb_eu.map((e: { title: string }, index: number) => (
									<option key={index} value={e.title}>{e.title}</option>
								))}
							</select>
						</p> */}
						<textarea value={adresseSeitenbetreiber} className="form-control" cols={40} rows={4} onChange={changeBetreiberAdresse}></textarea>
						<small className="form-text text-muted">{t("tracker-gen.input.section-address-site-operator.note")}</small>

					</div>

				) : (
					<div className="textfrage">
						<h5>{t("tracker-gen.input.section-email-privacy-officer.title")}</h5>
						<input type="email" className="form-control" value={emailDatenschutzbeauftragter} onChange={(event: any) => { setEmailDatenschutzbeauftragter(event.target.value) }} />
						<small className="form-text text-muted">{t("tracker-gen.input.section-email-privacy-officer.title")}</small>
					</div>
				)}

				<br />
				<h5 dangerouslySetInnerHTML={{__html: t("tracker-gen.input.section-tracker.title", {
					link: `<a target="_blank" rel="noopener noreferrer" href="https://developer.mozilla.org/de/docs/Tools/netzwerkanalyse">${t("tracker-gen.input.section-tracker.title-link-text")}</a>`,
					interpolation: { 
						escapeValue: false
					}
				})}}></h5>

				<CheckBoxes
					onChange={ (checked: string[]) => { setCheckedTracker(checked.map(e => parseInt(e))); }}
					checked={checkedTracker.map(e => String(e))}
					options={getTrackerItems({
						t,
						Trackers,
						checkedTracker,
						cnameCloakingDomains,
						updateCnameCloakingDomains,
						setCnameCloakingDomains
					})}
					/>

				<br />

				{(isBeschwerde()) ? (

					<div className="textfrage">

						<h5>{t("tracker-gen.input.section-time.title")}</h5>
						<p><input id="checkzeit" className="form-control" type="text" value={lastCheckDate} onChange={(event)=>{setLastCheckDate(event.target.value)}} /></p>

						<h5>{t("tracker-gen.input.section-documents.title")}</h5>

						<CheckBoxes
							onChange={setCheckedBelege}
							checked={checkedBelege.map(e => String(e))}
							options={(t("tracker-gen.input.section-documents.options", {
								returnObjects: true,
								interpolation: { escapeValue: false }
								
							}) as any).map((e: {body: string, value: string, link?: string}) => {
								if (!e.link) return e;
								return {
									value: e.value,
									after: (
										<span dangerouslySetInnerHTML={{
											__html: e.body.replace("[a]", `<a target="_blank" rel="noopener noreferrer" href="${e.link}">`).replace("[/a]", "</a>")
										}}></span>
									)
									
								}
							})}
							/>

						<br />

					</div>

				) : null}

				<h5>{t("tracker-gen.input.section-consent.title")}</h5>
				<p>{t("tracker-gen.input.section-consent.desc")}</p>

				<RadioButton value="ja" label={t("tracker-gen.input.section-consent.label-ja")} checkedValue={zustimmungsTyp} onChange={setZustimmungsTyp} />
				<RadioButton value="nein" label={t("tracker-gen.input.section-consent.label-nein")} checkedValue={zustimmungsTyp} onChange={setZustimmungsTyp} />
				<RadioButton
					value="minimal"
					label={t("tracker-gen.input.section-consent.label-minimal")}
					checkedLabel={t("tracker-gen.input.section-consent.label-minimal-note")}
					checkedValue={zustimmungsTyp}
					onChange={setZustimmungsTyp}
				/>
				<br />

				<ModalButton buttonLabel={t("tracker-gen.input.section-consent.examples.button")} modalTitle={t("tracker-gen.input.section-consent.title")} className="btn btn-info btn-sm">
					<figure>
						<img className="img-fluid" src="images/cookie_gültig.png" alt="" /><br />
						<figcaption>{t("tracker-gen.input.section-consent.examples.desc-valid")}</figcaption>
					</figure>
					<br /><br />
					<figure>
						<img className="img-fluid float-right" src="images/cookie_ungültig.png" alt="" /><br />
						<figcaption>{t("tracker-gen.input.section-consent.examples.desc-invalid")}</figcaption>
					</figure>
				</ModalButton>

				<br />

				<h5>{t("tracker-gen.input.section-adress-person-concerned.title")}</h5>
				<textarea value={adresseBetroffener} className="form-control" cols={40} rows={4} onChange={(event)=>{setAdresseBetroffener(event.target.value)}}></textarea>
				<p><small className="form-text text-muted">{t("tracker-gen.input.section-adress-person-concerned.note")}</small></p>

				<br />
				

			</div>

			<div className="gen-output">

				<h3>{t(`tracker-gen.output.title-${(isBeschwerde()) ? "complaint" : "request"}`)}</h3>

				<br />

				<ul className="actions">
					<li onClick={copyGenTextToClipboard}>{t(`tracker-gen.output.section-actions.copy`)}</li>
					<li onClick={openEmailProgram} >{t(`tracker-gen.output.section-actions.open-email`)}</li>
					<li onClick={e => generateICSFile(t, trackerUrl)} >{t(`tracker-gen.output.section-actions.reminder`)}</li>
				</ul>

				{(zustimmungsTyp === "ja" && clickedAnAction) ? (
					<div className="consent-warning alert alert-danger">
						{t(`tracker-gen.output.consent-warning`)}	
					</div>
				) : null}

				{(isBeschwerde()) ? (
					<div>
						<h5>{t(`tracker-gen.output.section-more-infos.title`)}</h5>
						<p>
							{t(`tracker-gen.output.section-more-infos.responsible-dpo`, {
								dsb: selectedDSB.title
							})}
							<br />
							{(selectedDSB.title !== "?") ? (<span>
								
								{t(`common:to`)} <a href={selectedDSB.internet} target="_blank" rel="noopener noreferrer" >{t(`tracker-gen.output.section-more-infos.complaint-form`)}</a>
							
							</span>) : null}
						</p>
					</div>
				) : null}

				<br />
				<h5>{t(`tracker-gen.output.section-meta-data.title`)}</h5>
				<p>
					{t(`tracker-gen.output.section-meta-data.to`)}: <a href={"maito:"+getActiveToEmail()}>{getActiveToEmail()}</a><br />
					{t(`tracker-gen.output.section-meta-data.subject`)}: <b>{getSubject()}</b>
				</p>

				<br />
				<h5>{t(`tracker-gen.output.section-gentext.title`)}</h5>

				<div className="emailText">
					{(isBeschwerde()) ? getComplaintText() : getRequestText()}
				</div>

			</div>
		
		</div>

	)
}