import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

import "../style/tracker.scss"
import { convertArrayToOrList } from '../utils';

export interface ITracker {
    title: string,
    url?: string | string[],
    request: string,
    complaint: string,
    cnameCloaking?: boolean,
    searchby?: string,
    note?: {
        image: string,
        title: string,
        body: string
    }
}

export interface ITrackers extends Array<ITracker>{}

function ListItem (props: {item: ITracker, index: number}) {

    const [t] = useTranslation();
    const [isItemOpen, setIsItemOpen] = useState(false);

    const url = (typeof props.item.url === "string") ? props.item.url : convertArrayToOrList(props.item.url || [])

    return (
        <li key={props.index} className={(isItemOpen) ? "show" : ""}>
            <div className="title" onClick={() => setIsItemOpen(!isItemOpen)}>
                {props.item.title} <span>({(props.item.cnameCloaking) ? t("tracker-database.cname-cloaking") : url})</span>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="24px" height="24px">
                    {(isItemOpen) ? (
                        <path d="M8.12 14.71L12 10.83l3.88 3.88c.39.39 1.02.39 1.41 0 .39-.39.39-1.02 0-1.41L12.7 8.71c-.39-.39-1.02-.39-1.41 0L6.7 13.3c-.39.39-.39 1.02 0 1.41.39.38 1.03.39 1.42 0z" />
                    ) : (
                        <path d="M8.12 9.29L12 13.17l3.88-3.88c.39-.39 1.02-.39 1.41 0 .39.39.39 1.02 0 1.41l-4.59 4.59c-.39.39-1.02.39-1.41 0L6.7 10.7c-.39-.39-.39-1.02 0-1.41.39-.38 1.03-.39 1.42 0z" />
                    )}
                </svg>
            </div>
            <div className="more">
                <p className="linebreaks">{props.item.request}</p>
                {(props.item.note) ? (
                    <div>
                        <p><b>{t("tracker-database.note")}</b></p>
                        <p className="linebreaks">{props.item.note.body}</p>
                        <img width="100%" alt={props.item.note.title} src={props.item.note.image}/>
                    </div>
                ) : null}
            </div>
        </li>
    )

}

export default function () {

    const [t] = useTranslation();

    const [filterby, setFilterBy] = useState("");

    const Tracker: ITracker[] = t('tracker', { returnObjects: true });

    function getFilteredTracker () {

        return Tracker.filter((trackerItem) => {

            return trackerItem.title.indexOf(filterby) > -1 || (trackerItem.url && trackerItem.url.indexOf(filterby) > -1);

        } );

    }

    return (
        <div className="tracker-list">
            
            <header>
                <div className="info">
                    <p className="linebreaks">
                        {t("tracker-database.desc")}
                    </p>
                    <div className="button">
                        <a href="https://codeberg.org/rufposten/Traecktor/issues" target="_blank" rel="noopener noreferrer"><button className="btn btn-primary">{t("tracker-database.add-new-tracker")}</button></a>
                    </div>
                </div>
                <input placeholder={t("tracker-database.filter-placeholder")} value={filterby} onChange={event => setFilterBy(event.target.value)}/>
            </header>

            <ul>
                {getFilteredTracker().map((e: ITracker, index: number) => (
                    <ListItem item={e} index={index} key={index}/>
                ))}
            </ul>

        </div>
    )

}