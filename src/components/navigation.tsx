import React, { useState } from 'react';
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next"

import SelectLanguage from "../forms/selectlanguage"

import "../style/navigation.scss"

import traecktor_logo_300px from "../images/traecktor_logo_300px.png"

export default function (props: { 
    locale: string,
    changeLanguage: {(event: React.ChangeEvent<HTMLSelectElement>): void}
}) {

    const [mobileNavOpen, setMobileNavOpen] = useState(false);
    const [t] = useTranslation();

    return (
        <div className="navbar navbar-expand-lg static-top">
            <div className="container">

                <Link className="navbar-brand" to="/">
                    <img alt="Logo" src={traecktor_logo_300px} />
                    Träcktor
                </Link>

                <button className="navbar-toggler" type="button" onClick={()=>{setMobileNavOpen(!mobileNavOpen)}} >
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="18px" height="18px"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M4 18h16c.55 0 1-.45 1-1s-.45-1-1-1H4c-.55 0-1 .45-1 1s.45 1 1 1zm0-5h16c.55 0 1-.45 1-1s-.45-1-1-1H4c-.55 0-1 .45-1 1s.45 1 1 1zM3 7c0 .55.45 1 1 1h16c.55 0 1-.45 1-1s-.45-1-1-1H4c-.55 0-1 .45-1 1z"/></svg>
                </button>

                <div className={"collapse navbar-collapse" + ((mobileNavOpen) ? " show" : "")} onClick={()=>{setMobileNavOpen(false)}} >
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item">
                            <Link className="nav-link" to="/faq">{t("navigation.tutorial-faq")}</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/tracker">{t("navigation.tracker-database")}</Link>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="https://rufposten.de/blog/spenden/">{t("navigation.donate")} <span role="img" aria-label="Herz">❤️</span></a>
                        </li>
                        <li className="nav-item">
                            <SelectLanguage simple={true} locale={props.locale} changeLanguage={props.changeLanguage} />     
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
}