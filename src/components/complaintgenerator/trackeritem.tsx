import React from 'react';

import CopyIcon from "./copyicon"
import ModalButton from "../../forms/modelbutton"

import {ITrackers, ITracker} from "../tracker"
import { TFunction } from 'i18next';

interface ITrackerProps {
    t: TFunction,
    updateCnameCloakingDomains: (event: React.ChangeEvent<HTMLInputElement>) => void,
    setCnameCloakingDomains: React.Dispatch<React.SetStateAction<{
        trackerTitel: string;
        domain: string;
        lastChanged: boolean;
    }[]>>,
    cnameCloakingDomains: {
        trackerTitel: string;
        domain: string;
        lastChanged: boolean;
    }[],
    checkedTracker: number[]
}

interface ITrackerItem {
    functions: ITrackerProps,
    tracker: ITracker,
    index: number
}

function TrackerItem (props: ITrackerItem) {

    const { cnameCloakingDomains, checkedTracker, t, updateCnameCloakingDomains, setCnameCloakingDomains } = props.functions;

    const urls = ((typeof props.tracker.url === "string") ? [props.tracker.url] : props.tracker.url) || [];	
    const cnameCloakingDomain = cnameCloakingDomains.find(e => e.trackerTitel === props.tracker.title);

    return (
        <div className="info">

        {(props.tracker.cnameCloaking) ? (
            
            <div>

                <i> {t("tracker-gen.input.section-tracker.searching-in-networktraffik")}&ensp;</i> 
                {props.tracker.searchby} <CopyIcon copytext={props.tracker.searchby || ""} />

                {(checkedTracker.indexOf(props.index) > -1) ? (

                    <input
                        autoFocus={(cnameCloakingDomain?.lastChanged) ? true : false}
                        placeholder={t("tracker-gen.input.section-tracker.cname-domain-placeholder")}
                        type="text" value={cnameCloakingDomain?.domain}
                        data-domainid={props.index}
                        onBlur={e => setCnameCloakingDomains(cnameCloakingDomains.map(e => {
                            e.lastChanged = false;
                            return e;
                        }))}
                        onChange={updateCnameCloakingDomains}/>
                    
                ) : null}

            </div>

        ) : null}

        {urls.map((url, index: number) => (

            <div key={index} style={{display: "unset"}}>

                {url} <CopyIcon copytext={url} />

                {(index < urls.length-1) ? (
                    (index === urls.length-2) ? (<b> oder </b>) : (<b>, </b>)
                ) : null}

                <br />

            </div>

        ))}
        </div>
    );
}

interface IgetTrackerItems extends ITrackerProps {
    Trackers: ITrackers
}

export function getTrackerItems (props: IgetTrackerItems) {

    return props.Trackers.map((tracker: ITracker, index: number) => {

        return {
            value: String(index),
            label: tracker.title,
            body: <TrackerItem tracker={tracker} index={index} functions={props} />,
            filterText: tracker.title + tracker.url,
            after: (tracker.note && (props.checkedTracker.indexOf(index) > -1)) ? (
                <div className="alert alert-warning alert-dismissible fade show" role="alert" style={{marginTop: "10px"}}>

                    <p>{tracker.note.body}</p>

                    <ModalButton buttonLabel={props.t("tracker-gen.input.section-tracker.show-example")} modalTitle={tracker.note.title} className="btn btn-info btn-sm">
                        <img width="100%" className="img-fluid" src={tracker.note.image} alt={tracker.title}/>
                        <br />
                    </ModalButton>

                </div>
            ) : null
        }
    })

}
