
![Träcktor-Logo](src/images/traecktor_logo_300px.png)
# Träcktor

Ein Offline-DSGVO-Beschwerdegenerator gegen Tracking-Einbettungen

Webseite: [https://träcktor.de/](https://träcktor.de/)

# Inhalt
* <a href="#screenshot">Screenshot</a>
* <a href="#entwicklung">Entwicklung</a>
* <a href="#todo">ToDo</a>
* <a href="#copyright-und-lizenz">Copyright und Lizenz</a>

## Screenshot
![Screenshot](screenshot.png)

## Entwicklung

### Anforderungen

Es wird [Node.js](https://nodejs.org/en/) benötigt um die Webseite zu kompilieren.


### Erste Schritte

1. Dieses Repository klonen
```
git clone https://codeberg.org/rufposten/Traecktor.git
```
2. In das `traecktor`-Basisverzeichnis wechseln
```
cd Traecktor
```
3. NodeJS installieren (nachfolgender Code ist für Debian)
```
sudo apt install nodejs npm
```
4. Benötigte Packete wie [React](https://reactjs.org) installieren mit
```
npm install
```
5. Live-Entwicklungsumgebung starten mit
```
npm start
``` 

## Ordner-Struktur

| Ordner / Datei | Beschreibung |
| ------ | ------------ |
| `/build` | Dieser Ordner wird von `React` erstellt und enthält den Produktions-Frontend-Code |
| `/public` | Enthält statischen Dateien wie Bilder |
| `/src` | Das Frontend der App in React und Typescript |
| `/src/components/complaintgenerator.tsx` | der Beschwerdegenerator |
| `/src/locate/[de]` | Sprachdateien und länderspezifische Informationen wie zB. die Kontakt-Daten der Datenschutzbehörden |
| `/src/locate/[de]/text.json` | Sprachdatei (enthält auch die Tracker) |
| `/src/locate/[de]/{dsb.json, plzToDsb.json}` | Datenschutzbehörden und die Zuordnung der Postleitzahlen |

## Production

1. Produktions-Build erstellen mit
```
npm run build
```
2. Webseite öffnen  

In das `build`-Verzeichniss wechseln und die `index.html`-Datei öffnen.

## ToDo

* Option: "meinen Namen auch gegenüber dem Websitebetreiber offenlegen"
* Erinnerungs-Links mit gespeicherten Optionen ?tracker=1,2,3


## Copyright und Lizenz
**Icons**  
[Fxemoji-Set](https://github.com/mozilla/fxemoji) von Mozilla unter [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/)

**Träcktor** is MIT licensed.
